#include <cmath>
#include <vector>

#pragma once

class StrangeAttractor
{
public:
    StrangeAttractor();
    virtual ~StrangeAttractor() = default;
    
    struct Position
    {
        double x,y,z;
        double dx,dy,dz;
        double t;   // seconds
    };

    void calculatePoints(double dt, double endTime);

    std::vector<Position> points;
};
