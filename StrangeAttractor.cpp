#include "StrangeAttractor.h"

StrangeAttractor::StrangeAttractor()
{
}

namespace {

// Differential Equation (based on "Dequan Li") plus Euler solver
StrangeAttractor::Position calcNextPoint(StrangeAttractor::Position prev, double deltaT)
{
    StrangeAttractor::Position next;
    next.dx = 40*(prev.y-prev.x)+0.16*prev.x*prev.z;
    next.dy = 55*prev.x + 20*prev.y - prev.x*prev.z;
    next.dz = 1.833*prev.z + prev.x*prev.y - 0.65*prev.x*prev.x;
    next.x = prev.x + deltaT*next.dx;
    next.y = prev.y + deltaT*next.dy;
    next.z = prev.z + deltaT*next.dz;
    next.t = prev.t + deltaT;
    return next;
}

}

void StrangeAttractor::calculatePoints(double dt, double endTime)
{
    // calculate the points
    Position pos = {1,1,1, 0,0,0, 0};
    for (size_t i=0; i<endTime/dt; i++)
    {
        pos = calcNextPoint(pos, dt);
        points.push_back(pos);
    }
}
