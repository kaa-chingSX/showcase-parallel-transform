#include <iostream>
#include <list>
#include <cmath>

template<typename T>
void print(T& d)
{
    for(auto a : d)
        std::cout << a << " ";
    std::cout << std::endl;
}

#include <algorithm>
#include <execution>
#include <functional>
#include <numeric>

#include "./Stopwatch.h"

    std::list<int> primes(int max)
    {
        std::list<int> candidates;
        for (int i=1; i < max; ++i)
            candidates.push_back(i);
    
        for (int div=2; div < sqrt(max); ++div)
        {
            auto iter = candidates.begin();
            while (iter!=candidates.end())
            {
                auto citer = iter;
                ++iter;
                if (*citer%div==0 && *citer!=div)
                    candidates.erase(citer);
            }
        }
        return candidates;
    }

    std::list<int> primesInit(int max)
    {
        std::list<int> candidates(max);
        int i = 0;
        for (auto c = candidates.begin(); c != candidates.end(); ++c, ++i)
            *c = i;

        for (int div = 2; div < sqrt(max); ++div)
        {
            auto iter = candidates.begin();
            while (iter != candidates.end())
            {
                auto citer = iter;
                ++iter;
                if (*citer % div == 0 && *citer != div)
                    candidates.erase(citer);
            }
        }
        return candidates;
    }

namespace cow
{

    std::vector<int> primes(int max)
    {
        std::vector<int> candidates (max);
        for (int i = 0; i < max; ++i)
            candidates[i]=i;

        int highest = candidates.size();
        for (int div = 2; div < sqrt(candidates.size()); ++div) {
            int t = 2;
            for (int i = 2; i < highest; i++) {
                int c = candidates[i];
                if (c == div || c % div != 0) {
                    candidates[t] = c;
                    t++;
                }
            }
            highest = t;
        }
        candidates.resize(highest);
        return candidates;
    }

}

std::list<int> primes2(int max)
{
    std::list<int> candidates;
    for (int i = 1; i < max; ++i)
        candidates.push_back(i);

    auto divptr = candidates.begin();
    ++divptr;
    auto div = *divptr;

    for (;div=*divptr, div*div<max; ++divptr )
    {
        auto iter = divptr;
        while (iter != candidates.end())
        {
            auto citer = iter;
            ++iter;
            if (*citer % div == 0 && *citer != div)
                candidates.erase(citer);
        }
    }
    return candidates;
}

std::vector<int> primesSwap(int max)
{
    std::vector<int> candidates;
    for (int i = 1; i < max; ++i)
        candidates.push_back(i);
    auto last = candidates.end();

    for (int div = 2; div < sqrt(max); ++div)
    {
        auto iter = candidates.begin();
        while (iter != last)
        {
            auto citer = iter;
            ++iter;
            if (*citer % div == 0 && *citer != div) {
                std::swap(*citer, *(last-1));
                last--;
                iter--;
            }
        }
    }
    return std::vector<int>(candidates.begin(), last);
}

namespace algo
{

    std::vector<int> primes(int max)
    {
        std::vector<int> candidates(max);
        auto last = candidates.end();
        std::iota(candidates.begin(), last, 1);




        for (int div = 2; div < sqrt(max); ++div)
        {
            last =
                std::remove_if(candidates.begin(), last,
                    [&div](int i) { return i % div == 0 && i != div; });
        }
        return std::vector<int>(candidates.begin(), last);
    }



    std::vector<int> primesAlgo(auto exec, int max)
    {
        std::vector<int> candidates(max);
        auto last = candidates.end();
        std::iota(candidates.begin(), last, 1);
    
        for (int div=2; div < sqrt(max); ++div)
        {
            last =
                std::remove_if(exec, 
                    candidates.begin(), last, 
                    [&div](int i){ return i%div==0 && i!=div; });
        }
        return std::vector<int>(candidates.begin(), last);
    }



    std::vector<int> primesAlgo2(auto exec, int max)
    {
        std::vector<int> candidates(max);
        auto last = candidates.end();
        std::iota(candidates.begin(), last, 1);

        for (int i=1; candidates[i] < sqrt(max); i++)
        {
            auto div = candidates[i];
            last =
                std::remove_if(exec,
                    candidates.begin(), last,
                    [&div](int i) { return i % div == 0 && i != div; });
        }
        return std::vector<int>(candidates.begin(), last);
    }



}

template <typename T>
void measurePrime(const std::string& description, std::function<T()> fie)
{
    Stopwatch s;
    s.start();
    T v = fie();
    s.stop();
    std::cout << description << "; primes size: " << v.size() << ", time: " << s.getElapsed<Stopwatch::Microseconds>() / 1000. << "ms." << std::endl;
}

int main()
{
    const int n = 1000*1000;
//    const int n = 50;

//    measurePrime<std::list<int>>(  "unoptimized, list-based        ", [n]() {return primes(n); }); // very unoptimized
    measurePrime<std::vector<int>>("unoptimized, vector-copy-based ", [n]() {return cow::primes(n); }); // uses vectors and copies
    measurePrime<std::vector<int>>("using algorithms               ", [n]() {return algo::primes(n); }); // quite optimized
    measurePrime<std::vector<int>>("execution::seq       algorithms", [n]() {return algo::primesAlgo2(std::execution::seq, n); });
    measurePrime<std::vector<int>>("execution::par       algorithms", [n]() {return algo::primesAlgo2(std::execution::par, n); });
    measurePrime<std::vector<int>>("execution::unseq     algorithms", [n]() {return algo::primesAlgo2(std::execution::unseq, n); });
    measurePrime<std::vector<int>>("execution::par_unseq algorithms", [n]() {return algo::primesAlgo2(std::execution::par_unseq, n); });
}
