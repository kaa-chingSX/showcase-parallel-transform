#include <iostream>
#include <string>
#include <thread>
#include <vector>

	void worker(int number) {
		std::this_thread::sleep_for(std::chrono::seconds(number));
		std::cout << number << " ";
	}

	int main(int argc, char* argv[]) {
		std::vector<std::thread> threads;

		for (int i = 1; i < argc; ++i)
			threads.emplace_back(worker, std::stoi(argv[i]));

		for (auto& t : threads)
			t.join();

		std::cout << std::endl;
	}
