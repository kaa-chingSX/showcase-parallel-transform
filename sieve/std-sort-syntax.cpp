#include <iostream>

#include <algorithm>
#include <vector>

void print(std::vector<int>& d)
{
    for(auto a : d)
        std::cout << a << " ";
    std::cout << std::endl;
}

int main()
{
    
    std::vector<int> data {9, 3, 6, 4, 2, 6, 8, 1, 1};
    std::sort(data.begin(), data.end());
 
    print(data);
    
    std::cout << "data size: " << data.size() << std::endl;

}
