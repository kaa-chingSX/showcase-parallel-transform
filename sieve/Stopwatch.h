//  
// This final class can be used for benchmarking:
//   Stopwatch s;
//   s.start();
//   <<run your code here>>
//   s.end();
//   std::cout << s.getElapsed<Stopwatch::Microseconds>()/1000. << std::endl;
// If you run start()-end() repeatedly, the getElapsed will be an average of all runs.
// Use reset() to reset all statistics, doing so while measuring will produce 'funny' results.
//
// (C) Vector Fabrics       2012-2016
//     High Tech Institute  2017-2022
// This software is in the public domain, feel free to do with it what you want, except claim you wrote it.
// Authored by Andrei Terechko and Klaas van Gend.


#pragma once

#include <chrono>
#include <cstdint>


class Stopwatch final
{
public:
  using Seconds = std::chrono::seconds;
  using Milliseconds = std::chrono::milliseconds;
  using Microseconds = std::chrono::microseconds;

  void start() noexcept;
  void stop() noexcept;
  void reset() noexcept;

  constexpr unsigned int getNumberOfMeasurements() const noexcept;

  template<typename Duration>
  [[nodiscard]] constexpr uint64_t getElapsed() const noexcept;

private:
  using Clock = std::chrono::steady_clock;

  Clock::time_point mTimeStart;
  Clock::time_point mTimeStop;

  Clock::duration mTimeTotal = Microseconds(0);
  unsigned int mNumberOfMeasurements = 0;
};


void Stopwatch::start() noexcept
{
  mTimeStart = Clock::now();
}


void Stopwatch::stop() noexcept
{
  mTimeStop = Clock::now();

  mTimeTotal += (mTimeStop - mTimeStart);
  mNumberOfMeasurements++;
}


void Stopwatch::reset() noexcept
{
  mTimeTotal = Microseconds(0);
  mNumberOfMeasurements = 0;
}


constexpr unsigned int Stopwatch::getNumberOfMeasurements() const noexcept
{
  return mNumberOfMeasurements;
}


template<typename Duration>
constexpr uint64_t Stopwatch::getElapsed() const noexcept
{
  return std::chrono::duration_cast<Duration>(mTimeTotal).count() / mNumberOfMeasurements;
}
