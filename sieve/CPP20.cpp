﻿#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <vector>
#include <string>


	std::mutex m;
	std::condition_variable_any cv;

	void worker(std::stop_token tok, int number) {
		std::unique_lock<std::mutex> lk(m);
		cv.wait_for(lk, tok, std::chrono::seconds(number), []() {return false; });
		std::cout << number << " ";
	}

	int main(int argc, char* argv[]) {
		std::vector<std::jthread> threads;
		for (int i = 1; i < argc; ++i)
			threads.emplace_back(worker, std::stoi(argv[i]));

		std::this_thread::sleep_for(std::chrono::seconds(4));
		std::cout << std::endl;
	}
