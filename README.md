# Example code to talk "Parallelization in C++: easy or not"

## What's this?
This is the gitlab repository showing the code for my talk.
Originally, I was a bit in a time crunch, so I first pushed for completion of the paper, now I'm preparing the demo of the code.

The source code originates from an example of the [Multicore Programming C++ training course](https://www.hightechinstitute.nl/courses/multicore-programming-in-c-training/?utm_campaign=ESEKongress-MULT1&utm_id=ESEKongress-MULT1) by the High Tech Institute.
The original source code showcases the viewing of the [Dequan-Li Strange Attractor](https://www.deviantart.com/chaoticatmospheres/art/Strange-Attractors-The-Dequan-Li-Attractor-376066584), a 3D differential equation that just looks good:

![the strange attractor shown using the full exercise code](images/strange-attractor-with-balls.png)

It calculates a pathway around the attractors as a 3D point cloud.
The code shown is a first version of the path calculation (strange.cpp) and the 3D rotations and flattening of the point cloud so it can be drawn, where each point turns into a ball.

The drawing and all optimizations that we're doing in the original exercise are not part of this repo - we're focussing on speedups using:
 * C++17
 * OpenMP
 * oneAPI oneTBB

Note that the code in this repository doesn't show OpenMP or oneTBB yet.

## How to compile
It's easy **LINUX, command line**:
* Clone the repository.
* `mkdir -p build`
* `cd build`
* `cmake ..`
  * if your standard compiler isn't C++20 yet, your need to add something like `-DCMAKE_CXX_COMPILER=g++-11`
* the build directory contains your executable `strange`, see below.

It's easy **Windows, Visual Studio 2022**:
* Clone the repository.
* open the project directory in Visual Studio
* In the `Project` menu, create the CMake cache
  * This will take considerable time (more than a minute!) because CMake also downloads and configures googletest
* In the `Build` menu, select `Build All`
  * This will also build all tests
* the `out\build\x64-Debug` directory contains your executable, `strange.exe`, see below.

It's easy **Windows, Visual Studio Code**:
* Make sure to have the 'CMake Tools' extension installed
* Clone the repository
* Open the folder, trust the authors
* In the CMake plugin, configure your compiler setup (I chose Visual Studio 17 x86-64)
* In the CMake plugin, click "Build All"
* the `build\Debug` directory contains your executable, `strange.exe`, see below.

## How to run
The executable needs one or more parameters, indicating how many elements to use.
The command line used for the benchmark was (Linux):
* `sudo cpupower frequency-set --min 1000M --max 1000M`
* `./strange 600 1000 2000 3000 6000 10000 100000 10000000 40000000`

The cpupower command makes benchmarking more reliable as it fixes all cores to a defined speed.
I chose this speed such that the CPU can handle the workload without throttling due to overheating.
Your system will feel slow after running this command.
* TODO: how to revert this!

