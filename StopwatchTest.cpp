#include "gtest/gtest.h"
#include "Stopwatch.h"

#include <thread>

using namespace std::literals;

TEST(Stopwatch, startSleepStop_measuresTimeBetween)
{
    // Arrange
    Stopwatch sw;

    // Act
    sw.start();
    std::this_thread::sleep_for(200ms);
    sw.stop();

    // Assert
    EXPECT_EQ(sw.getNumberOfMeasurements(), 1);
    EXPECT_GE(sw.getElapsed<Stopwatch::Milliseconds>(), 200) << "sleep should have been at least 200ms";
    EXPECT_LT(sw.getElapsed<Stopwatch::Milliseconds>(), 300) << "sleep should have been less than 300ms"; // INTERMITTENT: test might fail on very busy systems
}

TEST(Stopwatch, multiStartStop_multiMeasurement)
{
    // Arrange
    Stopwatch sw;

    // Act
    sw.start();    sw.stop();
    sw.start();    sw.stop();
    sw.start();    sw.stop();

    // Assert
    EXPECT_EQ(sw.getNumberOfMeasurements(), 3);
}

TEST(Stopwatch, reset_resetsMeasurements)
{
    // Arrange
    Stopwatch sw;

    // Act
    sw.start();
    sw.stop();
    sw.reset();

    // Assert
    EXPECT_EQ(sw.getNumberOfMeasurements(), 0);
    // getElapsed will error out: division by zero
}


