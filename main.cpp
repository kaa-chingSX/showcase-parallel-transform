#include "PointCloud.h"
#include "Stopwatch.h"
#include "StrangeAttractor.h"

#include <execution>
#include <iostream>
#include <thread>
#include <vector>

using namespace std::chrono_literals;

void measure(auto T, StrangeAttractor& sa, int type)
{
    std::this_thread::sleep_for(2s);
    std::cout << "rotate start count=" << sa.points.size() << " type=" << type << std::endl;
    Stopwatch rotateStopwatch;
    for(int i=0; i<7; i++)
    {
        PointCloud pc(sa.points);
        rotateStopwatch.start();
        if (type==3)
            pc.rotateHTo(T, 1.0);
        else
            pc.rotateHTo2(T, 1.0);
        rotateStopwatch.stop();
        std::cout << rotateStopwatch.getElapsed<Stopwatch::Microseconds>()/1000. << "ms. ";
        if (i==1)
            rotateStopwatch.reset();
    }
    std::cout << std::endl << "measured: " << rotateStopwatch.getElapsed<Stopwatch::Microseconds>()/1000. << "ms." << std::endl << std::endl;
}


void measure4(uint64_t count)
{
    //create points
    std::cout << "Creating " << count << " points..." << std::endl;
    StrangeAttractor sa;
    sa.calculatePoints(10.0/count, 10.0);
    std::cout << "Done creating." << std::endl << std::endl;

    std::cout << "Execution policy: SEQ, using original 3 transforms" << std::endl;
    measure(std::execution::seq, sa, 3);
    std::cout << "Execution policy: PAR, using original 3 transforms" << std::endl;
    measure(std::execution::par, sa, 3);
    std::cout << "Execution policy: SEQ, using joined transforms" << std::endl;
    measure(std::execution::seq, sa, 2);
    std::cout << "Execution policy: PAR, using joined transforms" << std::endl;
    measure(std::execution::par, sa, 2);
}

int main(int argc, const char* argv[])
{
    if (argc == 1)
    {
        std::cerr << "Error: you did not provide any number of elements as argument(s)." << std::endl;
        exit(1);
    }
    
    std::vector<long> number2Measure;
    for (int i=1; i<argc; ++i)
    {
        try {
            auto number = std::stol(argv[i]);
            if (number < 0)
               throw std::out_of_range("negative");
            number2Measure.push_back(number);
        }
        catch (std::invalid_argument&)
        {
            std::cerr << "Error parsing argument #" << i << " which is: `" << argv[i] << "` to a long. Bailing out." << std::endl;
            exit(1);
        }
	catch (std::out_of_range&)
	{
            std::cerr << "Error: argument #" << i << " negative or too large value." << std::endl;
            exit(1);
	}
    }
    
    for (auto number : number2Measure)
    {
        measure4(number);
    }
    
    return 0;
}
