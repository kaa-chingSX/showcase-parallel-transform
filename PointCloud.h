#include "StrangeAttractor.h"

#include <algorithm>
#include <cmath>
#include <execution>
#include <vector>

#pragma once

class PointCloud
{
public:
    using PosList = std::vector<StrangeAttractor::Position>;
    
    PointCloud(const PosList& list);
    virtual ~PointCloud() = default;
    
    struct Point2D
    {
        double x,y,depth;
    };

    std::vector<Point2D> rotateHTo(auto T, double angle_rad);
    std::vector<Point2D> rotateHTo2(auto T, double angle_rad);

private:
    struct Point3D
    {
        double x,y,z;
    };

private:
public:
    const PosList m_positions;
};



PointCloud::PointCloud(const PointCloud::PosList& list)
    : m_positions(list)
{
}

std::vector<PointCloud::Point2D> PointCloud::rotateHTo(auto T, double angle_rad)
{
    std::vector<Point3D> rotated(m_positions.size());
    
    double cosine = std::cos(angle_rad);
    double sine = std::sin(angle_rad);
    std::vector<PointCloud::Point2D> coords2D(m_positions.size());
    
    std::transform(T, m_positions.begin(), m_positions.end(),
        rotated.begin(), [cosine, sine](const StrangeAttractor::Position& orig) {
            return Point3D { cosine*orig.x-sine*orig.y,
                    cosine*orig.y+sine*orig.x, orig.z};
    });
    
    std::sort(T, rotated.begin(), rotated.end(), [](const Point3D& a, const Point3D& b) {
        return a.x > b.x;
    });
    
    std::transform(T, rotated.begin(), rotated.end(), coords2D.begin(),
                   [](const Point3D& pos) {
                       return Point2D{ pos.y - 0.2*pos.x, pos.z + 0.6*pos.x, 0};
    });
    
    return coords2D;
}

std::vector<PointCloud::Point2D> PointCloud::rotateHTo2(auto T, double angle_rad)
{
    double cosine = std::cos(angle_rad);
    double sine = std::sin(angle_rad);
    std::vector<PointCloud::Point2D> coords2D(m_positions.size());
    
    std::transform(T, m_positions.begin(), m_positions.end(),
        coords2D.begin(), [cosine, sine](const StrangeAttractor::Position& orig) {
            Point3D pos{ cosine*orig.x-sine*orig.y,
                    cosine*orig.y+sine*orig.x, orig.z};
            return PointCloud::Point2D{ pos.y - 0.2*pos.x, pos.z + 0.6*pos.x, pos.x};
    });
    
    std::sort(T, coords2D.begin(), coords2D.end(), [](const PointCloud::Point2D& a, const PointCloud::Point2D& b) {
        return a.depth > b.depth;
    });
    return coords2D;
}
